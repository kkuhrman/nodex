<?php
/******************************************************************************
 * @filesource: jobutil.php
 *
 * Job helper functions. Required by nodex.php script.
 *
 * @copyright:	Copyright © 2019 Kuhrman Technology Solutions LLC

 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
 
 require_once implode(DIRECTORY_SEPARATOR, array(dirname(__DIR__), 'vendor', 'autoload.php'));
 
 /**
  * Helper function redirects execution of script to outside job.
  * @return void
  */
 function redirectJob() {
	$success = FALSE;
	
	$job_status = executeJob();
	switch ($job_status) {
		default:
			break;
		case 0:
			$success = TRUE;
			break;	
	}
	return $success;
 }