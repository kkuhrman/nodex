<?php
/******************************************************************************
 * @filesource: rulesutil.php
 *
 * nodex Rules helper functions. Required by nodex.php script.
 *
 * @copyright:	Copyright © 2018 Kuhrman Technology Solutions LLC
 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

//
// Directory constants
//
define('DIR_RULES', 'rules');

//
// Rules global variables
//
global $Rules;
global $RuleNodeList;
global $rulesdir;
global $rulespath;

/**
 * Helper function verifies that a valid rules file exists either at the default
 * location or the path passed as a value to the -r option.
 * @return boolean TRUE if valid path is given for rules file otherwise FALSE.
 */
function verifyRulesPath() {
    global $rulesdir;
    global $rulespath;
    global $options;
    global $errors;
    global $workdir;
    $rules_file = NULL;
    $fs_checks = 0;
    
    //
    // Check for value passed to -f | --file option
    //
    if (isset($options["f"])) {
        $rules_file = $options["f"];
    }
    else if (isset($options["file"])) {
        $rules_file = $options["file"];
    }
    
    if (isset($rules_file)) {
    	//
		// Directory for rules file
		//
		$rulesdir = implode(DIRECTORY_SEPARATOR, array($workdir, DIR_RULES));
		if (file_exists($rulesdir) && is_dir($rulesdir) && is_readable($rulesdir)) {
			$fs_checks += 1;
		}
		else {
			$errors[] .= sprintf("Rules directory %s is not valid.",$rulesdir);
			goto END_FAIL;
		}
		
		//
		// Check for rules file
		//
		$rulespath = implode(DIRECTORY_SEPARATOR, array($rulesdir, $rules_file));
    	if (file_exists($rulespath) && is_readable($rulespath)) {
    		$fs_checks += 1;
    	}
	   else {
	   	$errors[] .= sprintf("Rules file path %s is not accessible or readable.",$rulespath);
	   	goto END_FAIL;
	   }
    }

	$fs_checks = TRUE;
	goto END_PASS;
	    
END_FAIL:
	$fs_checks = FALSE;
	
END_PASS:
    
    return $fs_checks;
}

/**
 * Helper function loads rule from XML file into DOM tree.
 */
function loadRules() {
	global $Rules;
	global $RuleNodeList;
	global $errors;
	$retval = FALSE;
	
	//
	// Load the rules document
	//
	$Rules = createDocumentFromFile($rulesdir);
	if (!isset($Rules)) {
		goto END_FAIL;
	}
		
	//
	// Load the rules
	//
	$RuleNodeList = $Rules->getElementsByTagName('node');
	if (!isset($RuleNodeList)) {
	    goto END_FAIL;
	}
	
	$retval = TRUE;
	goto END_PASS;
	
END_FAIL:
	$errors[] .= "Failed to load rules from rules file.";

END_PASS:
	return $retval;
}
