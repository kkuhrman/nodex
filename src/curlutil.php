<?php
/******************************************************************************
 * @filesource: curlutil.php
 *
 * CUrl and URL helper functions. Required by nodex.php script.
 *
 * @copyright:	Copyright © 2018 Kuhrman Technology Solutions LLC
 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

/**
 * Helper function verifies that a URL has been speficified or that an input
 * file exists either at the default location or the path passed as value to
 * the -f option.
 * @return boolean TRUE if a valid URL source exists otherwise FALSE.
 */
function verifyUrlSource() {
    global $infile;
    global $url;
    global $options;
    global $errors;
    $fs_checks = FALSE;
    
    if (isset($options["u"])) {
        $url = $options["u"];
        unset($infile);
        $fs_checks = TRUE;
        goto PASS_URL;
    }
    else if (isset($options["url"])) {
        $url = $options["url"];
        unset($infile);
        $fs_checks = TRUE;
        goto PASS_URL;
    }
    else {
        if (isset($options["f"])) {
            $infile = $options["f"];
            unset($url);
        }
        else if (isset($options["file"])) {
            $infile = $options["file"];
            unset($url);
        }
        $fs_checks = verifyInput();
    }
    if ($fs_checks === FALSE) {
        $errors[] .= sprintf("URL must passed as value to -u option or input file containing rules-based list of URLs may be passed as value to -f option.\n");
    }
    
PASS_URL:
    return $fs_checks;
}

/**
 * Helper function fetches the response body for the web resource given by URL
 * @param string $url web resource to GET
 * @param &FILE $fd Reference to output file descriptor must be open and writeable. 
 * @return mixed The response code for the HTTP GET request otherwise FALSE.
 */
function getWebResource($url, &$fd) {
    $retval = FALSE;
    
    //
    // Initialize cUrl session
    //
    $ch = curl_init($url);
    if ($ch === FALSE) {
        goto CURL_FAIL;
    }
    
    //
    // Set options
    //
    curl_setopt($ch, CURLOPT_FILE, $fd);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    
    
    //
    // Execute the session
    //
    $retval = curl_exec($ch);
    if ($retval) {
		$retval = curl_getinfo($ch,CURLINFO_RESPONSE_CODE);	
	 }
    curl_close($ch);
    fclose($fd);
    
CURL_FAIL:
    return $retval;
}