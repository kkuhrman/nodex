<?php
/******************************************************************************
 * @filesource: logutil.php
 *
 * Logging helper functions. Required by nodex.php script.
 *
 * @copyright:	Copyright © 2019 Kuhrman Technology Solutions LLC
 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

//
// Directory constants
//
define('DIR_LOG', 'log');

//
// Logging global variables.
//
global $LOGFILES;
global $logdir;
global $log_level;

//
// nodex logging severity levels
//
define('NODEX_LOG_LEVEL_NONE', 0);
define('NODEX_LOG_LEVEL_ERROR', 1);
define('NODEX_LOG_LEVEL_WARNING', 2);
define('NODEX_LOG_LEVEL_STATUS', 4);
define('NODEX_LOG_LEVEL_INFO', 8);
define('NODEX_LOG_LEVEL_DEBUG', 16);

//
// Default log level
//
$log_level = NODEX_LOG_LEVEL_ERROR;

//
// Log file names.
//
define('NODEX_LOG_URL', 'input.csv');
define('NODEX_LOG_OUT', 'output.csv');
define('NODEX_LOG_ERR', 'errors.csv');
$LOGFILES = array(
    NODEX_LOG_URL => NODEX_LOG_URL,
    NODEX_LOG_OUT => NODEX_LOG_OUT,
    NODEX_LOG_ERR => NODEX_LOG_ERR
);

 /**
 * Helper function closes log files if open
 * @return void
 */
function closeLogFiles() {
    global $LOGFILES;
    global $logdir;
    
    if (isset($logdir)) {
        foreach ($LOGFILES as $name => $fd) {
        		if (isset($fd) && is_resource($fd)) {
	            fclose($fd);        		
        		}
        }
    }
}

/**
 * Helper function checks if logging is turned off (NODEX_LOG_LEVEL_NONE) and 
 * otherwise checks for writeable log directory, opens log files for writing 
 * and changes default log level if -l (-log) option passed.
 *
 * @return boolean TRUE log files opened otherwise FALSE.
 */
function verifyLogging() {
    global $logdir;
    global $log_level;
    global $options;
    global $errors;
    global $LOGFILES;
    $logging = TRUE;
    
    if (isset($options["l"])) {
        $log_level = $options["l"];
    }
    else if (isset($options["log"])) {
        $log_level = $options["log"];
    }
    
    //
    // Is logging turned off?
    //
    if ($log_level === NODEX_LOG_LEVEL_NONE)
    {
    	goto LOG_FAIL;    
    }
    
    //
    // Check for a writeable log directory.
    //
    if (isset($logdir) && is_dir($logdir) && is_writeable($logdir)) {
	    //
	    // Open log files?
	    //
	    if (isset($logdir) && $logging) {
	        $filename = implode(DIRECTORY_SEPARATOR, array($logdir, NODEX_LOG_URL));
	        $LOGFILES[NODEX_LOG_URL] = fopen($filename, "a");
	        if ($LOGFILES[NODEX_LOG_URL] === FALSE) {
	            $errors[] .= sprintf("Could not open log file %s.\n",$filename);
	            goto LOG_FAIL;
	        }
	        $filename = implode(DIRECTORY_SEPARATOR, array($logdir, NODEX_LOG_OUT));
	        $LOGFILES[NODEX_LOG_OUT] = fopen($filename, "a");
	        if ($LOGFILES[NODEX_LOG_OUT] === FALSE) {
	            $errors[] .= sprintf("Could not open log file %s.\n",$filename);
	            goto LOG_FAIL;
	        }
	        $filename = implode(DIRECTORY_SEPARATOR, array($logdir, NODEX_LOG_ERR));
	        $LOGFILES[NODEX_LOG_ERR] = fopen($filename, "a");
	        if ($LOGFILES[NODEX_LOG_ERR] === FALSE) {
	            $errors[] .= sprintf("Could not open log file %s.\n",$filename);
	            goto LOG_FAIL;
	        }
	    }    
	    goto LOG_PASS;
    }
    else {
    	$errors[] .= sprintf("Failed to verify logging directory %s.\n",$logdir);
	   goto LOG_FAIL;
    }
    
LOG_FAIL:
    $logdir = NULL;
    $logging = FALSE;
    
LOG_PASS:
    return $logging;
}

/**
 * Helper function writes given string to a log file with timestamp.
 * @param string $log Name of log file to write to.
 * @param string $msg Message to log.
 * @return void
 */
function writeLogFile ($log, $msg) {
    global $LOGFILES;
    global $logdir;
    
    if (isset($logdir) && isset($LOGFILES[$log])) {
        /**
         * @todo: DTZ in Rules file
         */
        $Dtz = new DateTimeZone('America/Chicago');
        $Now = new DateTime("now", $Dtz);
        $line = array(
            date_format($Now, "y-m-d"),
            date_format($Now, "H:i"),
            $msg
        );
        fputcsv($LOGFILES[$log],$line);   
    }
}