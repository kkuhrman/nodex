<?php
/******************************************************************************
 * @filesource: domutil.php
 *
 * DOM helper functions. Required by nodex.php script.
 *
 * @copyright:	Copyright © 2018 Kuhrman Technology Solutions LLC
 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

//
// XHTML constants
//
define('XHTML_1_1_QUALIFIED_NAME', 'html');
define('XHTML_1_1_PUBLIC_ID', "-//W3C//DTD XHTML 1.1//EN");
define('XHTML_1_1_SYSTEM_ID', "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd");
define('XHTML_1_1_NAMESPACE_URI', "http://www.w3.org/1999/xhtml");

//
// XPath query format strings
//
define('XPATH_QUERY_FIND_COMMENT_FMT', "comment()[. = '%s'][1]/following::*[1]");
define('XPATH_QUERY_FIND_CLASS_FMT', "//*[@class='%s']");
define('XPATH_QUERY_FIND_ID_FMT', "//*[@id='%s']");

/**
 * Helper function appends node(s) to given document based on given rule.
 * @param DOMDocument &$Document Document to append nodes to.
 * @param DOMNodeList $Nodes Nodes to clone and append to document.
 * @param DOMNode $Rule Rule describing how to append node(s).
 * @param int Number of nodes appended.
 */
function appendNodes(DOMDocument &$Document, DOMNodeList $Nodes, DOMNode $Rule) {
    $node_count = 0;
    global $errors;
    
    //
    // Disable libxml errors and fetch error information as needed
    //
    libxml_use_internal_errors(TRUE);
    
    foreach ($Nodes as $Node) {
        //
        // Clone content body and associate with empty document
        //
        $Element = $Document->importNode($Node, TRUE);
        if (!isset($Element)) {
            goto END_FAIL;
        }
        
        //
        // Append content body to empty document
        //
        /**
         * 
         * @todo: apply the rule here
         */
        $NodeList = $Document->getElementsByTagName('body');
        if (isset($NodeList)) {
            $bodyElement = $NodeList->item(0);
            if (isset($bodyElement)) {
                $bodyElement->appendChild($Element);
            }
            else {
                goto END_FAIL;
            }
        }
        else {
            goto END_FAIL;
        }
        $node_count++;
    }
    
END_FAIL:
    //
    // @see call to at begin of function
    //
    foreach (libxml_get_errors() as $error) {
        //
        // log any libxml errors
        //
        $errorMsg = sprintf("libxml error in %s: %s", __FILE__, $error->message);
        isset($error->column) ? $errorMsg .= sprintf(" (column %d)", $error->column) : NULL;
        $errors[] = $errorMsg;
    }
    
    libxml_clear_errors();

END_PASS:

    return $node_count;
}

/**
 * Create XHTML DOM Document
 *
 * @param string $namespaceURI  The namespace URI of the document element to create.
 * @param string $qualifiedName The qualified name of the document element to create.
 * @param string $publicId      The external subset public identifier.
 * @param string $systemId      The external subset system identifier.
 * @param array $documentProperties Array[DOMDocument property name => DOMDocument property value]
 * @return DOMDocument
 */
function createDocument(
    $namespaceURI = NULL,
    $qualifiedName = 'html',
    $publicId = NULL,
    $systemId = NULL,
    $documentProperties = NULL
    ) {
        
        //
        // Create doc type
        //
        $DOMImpl = new DOMImplementation();
        $DocType = $DOMImpl->createDocumentType(
            $qualifiedName,
            $publicId,
            $systemId
        );
        
        //
        // Create document
        //
        $Document = $DOMImpl->createDocument(
            $namespaceURI,
            $qualifiedName,
            $DocType
        );
        
        //
        // Set defaults
        //
        $Document->formatOutput = TRUE;
        $Document->strictErrorChecking = FALSE;
        $Document->preserveWhiteSpace = FALSE;
        $Document->validateOnParse = FALSE;
        
        //
        // Allow override of defaults
        //
        if (isset($documentProperties) && is_array($documentProperties)) {
            foreach($documentProperties as $propertyName => $propertyValue) {
                if (property_exists($Document, $propertyName)) {
                    $Document->$propertyName = $propertyValue;
                }
            }
        }
        
        return $Document;
}

/**
 * Creates an empty XHTML DOMDocument
 * @return DOMDocument
 */
function createEmptyDocument() {
    
    global $errors;
    
    //
    // Disable libxml errors and fetch error information as needed
    //
    libxml_use_internal_errors(TRUE);
    
    //
    // Document
    //
    $Document = createDocument(
            XHTML_1_1_NAMESPACE_URI,
            XHTML_1_1_QUALIFIED_NAME,
            XHTML_1_1_PUBLIC_ID,
            XHTML_1_1_SYSTEM_ID
        );
    
    //
    // <html> Element
    //
    $htmlElement = $Document->createElement('html');
    $htmlElement = $Document->appendChild($htmlElement);
    if (!isset($htmlElement)) {
        goto END_FAIL;
    }
    
    //
    // <head> Element
    //
    $headElement = $Document->createElement('head');
    $headElement = $htmlElement->appendChild($headElement);
    if (!isset($headElement)) {
        goto END_FAIL;
    }
    
    //
    // <body> Element
    //
    $bodyElement = $Document->createElement('body');
    $bodyElement = $htmlElement->appendChild($bodyElement);
    if (!isset($bodyElement)) {
        goto END_FAIL;
    }
    
    goto END_PASS;
    
END_FAIL:
    //
    // @see call to at begin of function
    //
    foreach (libxml_get_errors() as $error) {
        //
        // log any libxml errors
        //
        $errorMsg = sprintf("libxml error in %s: %s", __FILE__, $error->message);
        isset($error->column) ? $errorMsg .= sprintf(" (column %d)", $error->column) : NULL;
        $errors[] = $errorMsg;
    }
    
    libxml_clear_errors();

END_PASS:
    return $Document;    
}

/**
 * Create XHTML DOM Document from XHTML stored in a file.
 *
 * @param string $fullPath  Full path to template file.
 * @param string $qualifiedName The qualified name of the document element to create.
 * @param string $publicId      The external subset public identifier.
 * @param string $systemId      The external subset system identifier.
 * @param array $documentProperties Array[DOMDocument property name => DOMDocument property value]
 *
 * @return DOMDocument
 */
function createDocumentFromFileHtml($fullPath) {
    
    $Document = NULL;
    $realPath = realpath($fullPath);
    if (file_exists($realPath)) {
//         $Document = createDocument(
//             XHTML_1_1_NAMESPACE_URI,
//             XHTML_1_1_QUALIFIED_NAME,
//             XHTML_1_1_PUBLIC_ID,
//             XHTML_1_1_SYSTEM_ID
//         );
        $Document = new DOMDocument();
        @$Document->loadHTMLFile($realPath);
//         var_dump($Document);
    }
    return $Document;
}

/**
 * Create XML DOM Document from XML stored in a file.
 * @param string $fullPath  Full path to template file.
 * @return DOMDocument
 */
function createDocumentFromFile($fullPath) {
    $Document = NULL;
    $realPath = realpath($fullPath);
    if (file_exists($realPath)) {
        $Document = new DOMDocument();
        @$Document->loadHTMLFile($realPath);
    }
    return $Document;
}

/**
 * Extract nodes from a document based on the given rule and return as a list
 * @param DOMDocument $Document The document to search
 * @param DOMNode $Rule The rule used to search for nodes within the document.
 * @return DOMNodeList A list of all the nodes meeting search criteria
 */
function extractNodes(DOMDocument $Document, DOMNode $Rule) {
    global $errors;
    
    //
    // Disable libxml errors and fetch error information as needed
    //
    libxml_use_internal_errors(TRUE);
    
    $NodeList = new DOMNodeList();
    
    //
    // Decision tree for applying the Rule
    // 
    if ($Rule->hasAttributes()) {
        //
        // Read the attributes
        //
        $id = $Rule->getAttribute("id");
        $class = $Rule->getAttribute("class");
        $name = $Rule->getAttribute("name");
        
        //
        // 1. Query on id attribute?
        //
        if ($id !== '') {
            $query = sprintf(XPATH_QUERY_FIND_ID_FMT, $id);
            
            /**
             * @todo: filter out by class if applies
             */
            
            /**
             * @todo: filter out by tag/name if applies
             */
            
        }
        
        //
        // 2. Query on class attribute?
        //
        else if ($class !== '') {
            $query = sprintf(XPATH_QUERY_FIND_CLASS_FMT, $class);
            
            /**
             * @todo: filter out by tag/name if applies
             */
        }
        
        /**
         * @todo: 3. Query on name
         */
        
        //
        // Execute the query
        //
        $xpath = new DOMXPath($Document);
        $NodeList = $xpath->query($query);
    }
    
    /**
     * @todo: enforce the following rules if they apply
     * child, sibling or parent?
     * min/max?
     * node offset
     */
    
END_FAIL:
    //
    // @see call to at begin of function
    //
    foreach (libxml_get_errors() as $error) {
        //
        // log any libxml errors
        //
        $errorMsg = sprintf("libxml error in %s: %s", __FILE__, $error->message);
        isset($error->column) ? $errorMsg .= sprintf(" (column %d)", $error->column) : NULL;
        $errors[] = $errorMsg;
    }
    
    libxml_clear_errors();
    
END_PASS:
    return $NodeList;
}
