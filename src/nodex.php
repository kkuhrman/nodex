<?php
/**
 * @filesource: nodex.php
 *
 * PHP CLI scripting utilities for WWW resource batch jobs.
 *
 * nodex is a DOM/XML manipulation utility written in PHP. The purpose of nodex
 * is to separate batch job specific routines - such as data transformation, 
 * content creation and management, etc - from generic routines such as HTTP 
 * requests, opening and writing files, etc.
 *
 * nodex uses the cURL library to access resources on the WWW and the DOM/XML 
 * libraries to navigate and manipulate the response body. The resultant output 
 * can be streamed to stdout, one or more files or inserted into a DOM tree.
 *
 * Run [#]php nodex.php -h (or --help) for basic syntax help.
 * or @see: https://bitbucket.org/kkuhrman/nodex/wiki/Home for more help.
 *
 * @copyright:	Copyright © 2018 Kuhrman Technology Solutions LLC
 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/

require_once 'curlutil.php';
require_once 'domutil.php';
require_once 'fileutil.php';
require_once 'logutil.php';
//require_once 'jobutil.php';
require_once 'rulesutil.php';
require_once 'options.php';

//
// Version number
//
define('VERSION_MAJOR', 1);
define('VERSION_MINOR', 0);
define('VERSION_REV', 0);

//
// Error messages saved to this array.
//
global $errors;
$errors = array();

//
// Response to stdout.
//
global $response;
$response = 
"-------------------------------------------------
nodex DOM node extraction utility
Copyright © 2018 Kuhrman Technology Solutions LLC
--------------------------------------------------";

//
// Container for options passed on command line
//
global $options;

//
// Options defaults
//
global $indir;
global $jobdir;
global $logdir;
global $outdir;
global $tmpdir;
global $workdir;
$indir = DIR_IN;
//$jobdir = DIR_JOBS;
$logdir = DIR_LOG;
$outdir = DIR_OUT;
$rulesdir = DIR_RULES;
$tmpdir = DIR_TMP;
$workdir = NULL;

//
// Stores single URL in case -i option passed
//
global $url;

//
// Stores output file names and corresponding URL(s)
//
define('OUTPUT_TEMPFILE', 'temp');
define('OUTPUT_FILENAME', 'file');
define('OUTPUT_URL', 'url');
global $OUTPUTS;
$OUTPUTS = array();

//
// Echoes helpful hints to stdout
//
function outputHelp() {
    global $response;    
    global $helpopts;
    
    $response .= "\nSyntax [#]php -f ./path/to/nodex.php [OPTIONS]'\n\n";
    $response .= "OPTIONS\n";
    foreach($helpopts as $optcode => $optstr) {
    	$response .= sprintf("%s\t%s\n", $optcode, $optstr);
    }
    $response .= "\n";
    $response .= "Output files are written to the output directory. The default output directory\n" . 
                 "is ./nodex/out. Default behavior is to give output file(s) a file system\n" . 
                 "friendly name derived from the input url(s), but this behavior can be changed\n" .
                 "in the rules file.\n\n" .
                 "For more information see https://bitbucket.org/kkuhrman/nodex/wiki/Home\n\n";
}

//
// Launches a job in a separate script file and reports results.
//
function outputJob() {
	return redirectJob();
}

//
// Echoes version number
//
function outputVersion() {
    global $response;
    $response .= sprintf("\nVersion %d.%d.%d\n",
        VERSION_MAJOR,
        VERSION_MINOR,
        VERSION_REV);
}

/*******************************************************************************
 * Begin runtime script
 *******************************************************************************/

//
// Get options from command line
//
$options = getopt($shortopts, $longopts);

//
// Help and Version options cannot be combined with others
//
if (isset($options["h"]) || isset($options["help"])) {
    /**
     * @todo: Check if basic help or specific
     */
    outputHelp();
    goto END_PASS;
}
if (isset($options["v"]) || isset($options["version"])) {
    outputVersion();
    goto END_PASS;
}

//
// Check for the working directory.
//
if (verifyWorkingDirectory() === FALSE) {
	goto END_FAIL;
}

//
// Check for logging
//
if (verifyLogging() === FALSE) {
	goto END_FAIL;
}

//
// Check if the path given for the rules file references a readable file.
//
if (verifyRulesPath() === FALSE) {
	goto END_FAIL;
}

//
// @todo: remaining verifications
//
$response = "nodex exiting...\n";
goto END_PASS;

END_FAIL:
	foreach ($errors as $errno => $errmsg) {
	    writeLogFile(NODEX_LOG_ERR, $errmsg);
	    $response .= sprintf("\n%s", $errmsg);
	}
outputHelp();


END_PASS:
	closeLogFiles();
	echo $response;
	exit;

/*******************************************************************************
 * End runtime script
 /*****************************************************************************/