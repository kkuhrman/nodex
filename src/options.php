<?php
/******************************************************************************
 * @filesource: options.php
 *
 * Command line options. Required by nodex.php script.
 *
 * @copyright:	Copyright © 2019 Kuhrman Technology Solutions LLC
 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

//
// Container for options passed on command line
//
global $options;

//
// Default directories
//
global $indir;
global $infile;
global $jobdir;
global $jobfile;
global $logdir;
global $outdir;
global $rulesdir;
global $tmpdir;
global $workdir;

//
// Short options
//
global $shortopts;
$shortopts = "d:";
$shortopts .= "f:";
$shortopts .= "i:";
$shortopts .= "j:";
$shortopts .= "l:";
$shortopts .= "o:";
$shortopts .= "hOv";

//
// Long options
//
global $longopts;
$longopts = array(
    "directory:",
    "file:",
    "input:",
    "job",
    "log",
    "output:",
    "help::",
    "overwrite",
    "version"
);

//
// Options help strings
//
global $helpopts;
$helpopts = array(
	"-d\t--directory" => "Specify parent of all input/output directories. Default is present working directory.",
	"-f\t--file     " => "Specify name of rules file to use. Default is empty.",
	"-h\t--help     " => "Print the nodex help message.",
	"-i\t--input    " => "Specify a single, properly-formed URL as input. Default is empty.",
	"-j\t--job      " => "Specify file name of batch job script. Default is empty.",
	"-l\t--log      " => "Specify log event severity level. Default is LOG_ERROR.",
	"-o\t--output   " => "Specify file name of output. Default varies (see below).",
	"-O\t--overwrite" => "Forces overwrite of existing file(s) in event of name conflict.",
	"-v\t--version  " => "Print the current version number.",
);