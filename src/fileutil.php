<?php
/******************************************************************************
 * @filesource: fileutil.php
 *
 * File system helper functions. Required by nodex.php script.
 *
 * @copyright:	Copyright © 2018 Kuhrman Technology Solutions LLC
 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

//
// Directory constants
//
define('DIR_IN','in');
define('DIR_JOBS', 'jobs');
define('DIR_OUT','out');
define('DIR_TMP','tmp');
define('DIR_WORK', 'pwd');

/**
 * Helper function verifies if user has passed a value for working directory
 * for the -d | --directory option and prepends that to all input/output paths.
 * Otherwise, the present working directory is used. Once working directory is
 * established as valid, the helper function continues to verify the nodex 
 * project tree structure within given directory.
 */
function verifyWorkingDirectory() {
	global $errors;
	global $indir;
	global $jobdir;
	global $logdir;
	global $outdir;
	global $rulesdir;
	global $tmpdir;
	global $options;
	global $workdir;
	$fs_checks = 0;
	
	//
	// Working directory should only be set by this function.
	// This check should indicate that this function has already
	// been successfully called and all subsequent calls are 
	// redundant.
	//
	if (isset($workdir)) {
		$fs_checks = TRUE;
		goto END_PASS;	
	}
	
	//
	// Check for value passed to -d | --directory option.
	//
	if (isset($options["d"]) || isset($options["directory"])) {
		//
		// get value of -j option
		//		
		isset($options["d"]) ? $workdir = $options["d"] : $workdir = $options["directory"];
		
		//
		// Check that given directory is valid and meets requirements.
		//
		if (file_exists($workdir) && is_dir($workdir) && is_writeable($workdir)) {
			$fs_checks += 1;
		 }
		 else {
		 	$errors[] .= sprintf("Working directory %s is not valid.",$workdir);
		 	goto END_FAIL;
		 }
	}
	else {	
		//
		// Assume the present working directory.
		//
		$workdir = getcwd();
		if (file_exists($workdir) && is_dir($workdir) && is_writeable($workdir)) {
			$fs_checks += 1;
		}
		else {
			$errors[] .= sprintf("Working directory %s is not valid.",$workdir);
			goto END_FAIL;
		}
	}
	
	//
	// Remove any symbolic links and trailing delimiters
	//
	$workdir = realpath($workdir);

	//
	// Verify the remaining nodex project tree structure
	//
	
	//
	// Directory for input file(s)
	// @todo: this is not required unless called for in a rules file
	//
	$indir = implode(DIRECTORY_SEPARATOR, array($workdir, DIR_IN));
	if (file_exists($indir) && is_dir($indir) && is_readable($indir)) {
		$fs_checks += 1;
	}
	else {
		$errors[] .= sprintf("Input directory %s is not valid.",$indir);
		goto END_FAIL;
	}
	
	//
	// Directory for job script(s)
	// @todo: this is not always required
	//
	$jobdir = implode(DIRECTORY_SEPARATOR, array($workdir, DIR_JOBS));
	if (file_exists($jobdir) && is_dir($jobdir) && is_readable($jobdir)) {
		$fs_checks += 1;
	}
	else {
		$errors[] .= sprintf("Jobs directory %s is not valid.",$jobdir);
		goto END_FAIL;
	}
	
	//
	// Directory for log file(s)
	// @todo: this is not required if log level is set to NODEX_LOG_LEVEL_NONE
	//
	$logdir = implode(DIRECTORY_SEPARATOR, array($workdir, DIR_LOG));
	if (file_exists($logdir) && is_dir($logdir) && is_writeable($logdir)) {
		$fs_checks += 1;
	}
	else {
		$errors[] .= sprintf("Logging directory %s is not valid.",$logdir);
		goto END_FAIL;
	}
	
	//
	// Directory for output file(s)
	// @todo: output may likely always be required but could also go to stdout
	//        be certain whether this is absolutely required in all cases
	//
	$outdir = implode(DIRECTORY_SEPARATOR, array($workdir, DIR_OUT));
	if (file_exists($outdir) && is_dir($outdir) && is_writeable($outdir)) {
		$fs_checks += 1;
	}
	else {
		$errors[] .= sprintf("Output directory %s is not valid.",$outdir);
		goto END_FAIL;
	}
	
	//
	// Directory for temporary file(s)
	// @todo: be certain this is required in all may cases
	//
	$tmpdir = implode(DIRECTORY_SEPARATOR, array($workdir, DIR_TMP));
	if (file_exists($tmpdir) && is_dir($tmpdir) && is_writeable($tmpdir)) {
		$fs_checks += 1;
	}
	else {
		$errors[] .= sprintf("Temp directory %s is not valid.",$tmpdir);
		goto END_FAIL;
	}
	
	//
	// Everything looks good
	//
	$fs_checks = TRUE;
	goto END_PASS;
	
	
END_FAIL:
	$fs_checks = FALSE;
	$workdir = NULL;

END_PASS:
	return $fs_checks;
}

/**
 * Helper function opens specified input file. Logs error message on fail.
 * @param string $infile absolute or relative path name of input file.
 * @param @out &FILE $fin Reference to input file descriptor
 * @return FILE Input file descriptor or NULL if failed to access.
 */
function openInput($infile, &$fin) {
    if (!isset($fin)) {
        if (file_exists($infile) && is_readable($infile)) {
            $fin = fopen($infile, "r");
        }
        if (!isset($fin)) {
            global $errors;
            $errors[] = sprintf("Input file %s could not be opened.", $infile);
        }
    }
    return $fin;
}

/**
 *  Helper function opens specified output file. Logs error message on fail.
 *  @param string $outfile absolute or relative path name of output file.
 *  @param &FILE $fout Reference to output file descriptor.
 *  @return FILE Output file descriptor or NULL if failed to open.
 */
function openOutput($outfile, &$fout) {
    if (!isset($fout)) {
        //
        // Check if file already exists and if so is the truncate option set.
        //
        if (file_exists($outfile) && is_writeable($outfile)) {
            global $options;
            if (!isset($options["t"])) {
                global $errors;
                $errors[] = sprintf("Output file %s already exists. Use -t or --truncate option to overwrite.", $outfile);
                goto OPEN_OUT_FAIL;
            }
        }
        $fout = fopen($outfile, "w");
    }
OPEN_OUT_FAIL:
    return $fout;
}

/**
 * Helper function checks if value is passed for -j | --job option and 
 * verifies that any given value is a valid job script. 
 */
function verifyJob() {
	global $errors;	
	global $jobdir;
	global $options;
	$jobpath = NULL;
	$fs_checks = FALSE;
	
	if (verifyWorkingDirectory() === FALSE) {
		goto END_FAIL;	
	}
	
	//
	// Check if value for -j | --job is passed.
	//
	if (isset($options["j"]) || isset($options["job"])) {
		//
		// get value of -j option
		//		
		isset($options["j"]) ? $job = $options["j"] : $job = $options["job"];
		
		//
      // Set absolute path of job directory off working directory.
      //
      
      	$jobdir = implode(DIRECTORY_SEPARATOR, array($workdir, DIR_JOBS));
      	$jobpath = implode(DIRECTORY_SEPARATOR, array($jobdir, $job));
      	if (file_exists($jobpath) && is_executable($jobpath)) {
      		require_once $job;
			   $fs_checks = TRUE;
			   goto END_PASS;
      	}
	}
	
END_FAIL:
	$errmsg = "\njob '" . $job . "' not found in\n" . $job . "\n";
	foreach($job_search_paths as $jobpath)	{
		$errmsg .= $jobpath . "\n";	
	}
	$errors[] = $errmsg;
	
END_PASS:
	
	return $fs_checks;
}

/**
 * Helper function verifies that a valid temporary directory exists and is
 * writeable either at default location or the path passed as value to the
 * -d option.
 * @return boolean TRUE if valid temporary directory exists otherwise FALSE.
 */
function verifyTmpDir() {
    global $tmpdir;
    global $options;
    global $errors;
    $fs_checks = FALSE;
    
   //
   // Set absolute path of temporary directory off working directory.
   //
   if (isset($workdir)) {
		$tmpdir = implode(DIRECTORY_SEPARATOR, array($workdir, DIR_TMP));
	}
	
    if (file_exists($tmpdir) && is_writeable($tmpdir) && is_dir($tmpdir)) {
        $fs_checks = TRUE;
    }
    else {
        $errors[] .= sprintf("Temporary directory %s must exist AND be writeable.",$tmpdir);
    }
    
    return $fs_checks;
}

/**
 * Helper function verifies that a valid output directory exists and is
 * writeable either at the default location or the path passed as value to
 * the -o option.
 * @return boolean TRUE if valid output directory exists otherwise FALSE.
 */
function verifyOutDir() {
    global $outdir;
    global $options;
    global $errors;
    $fs_checks = FALSE;
    
//
   // Set absolute path of output directory off working directory.
   //
   if (isset($workdir)) {
		$outdir = implode(DIRECTORY_SEPARATOR, array($workdir, DIR_OUT));
	}
	
    if (isset($options["o"])) {
        $outdir = $options["o"];
        $fs_checks = TRUE;
    }
    else if (isset($options["output"])) {
        $outdir = $options["output"];
        $fs_checks = TRUE;
    }
    if (file_exists($outdir) && is_writeable($outdir) && is_dir($outdir)) {
        $fs_checks = TRUE;
    }
    if ($fs_checks === FALSE) {
        $errors[] .= sprintf("Output directory '%s' must exist AND be writeable.",$outdir);
    }

    return $fs_checks;
}

/**
 * Helper function generates a local path based off given URL
 * @param string $parent Full path to parent dir must exist and be writeable.
 * @param string $url URL to base local path on
 * @return string Local path based on given URL or NULL in case of fail condition.
 */
function createLocalPathFromUrl($parent, $url) {
    global $options;
    global $errors;
    $local_path = NULL;
    
    $url_parts = parse_url($url);
    
    if ($url_parts === FALSE) {
		$errors[] = sprintf("URL %s appears to be malformed.", $url);		
		goto END_FAIL;    
    }
    
    $len = strlen($url);
    $pos = strpos($url, "www.");
    $STRIP = 4;
    $path = "";
    
    if ($pos !== FALSE) {
        $path = substr($url, ($pos + $STRIP), $len - ($pos + $STRIP));
    }
    else {
        $pos = strpos($url, "://");
        $STRIP = 3;
        if ($pos !== FALSE) {
            $path = substr($url, ($pos + $STRIP), $len - ($pos + $STRIP));
        }
        else {
            //
            // Assume URL may still be valid e.g. google.com
            //
            $path = $url;
        }
    }
    $pathparts = explode('/', $path);
    $bottom_hierarchy = count($pathparts) - 1;
    $subdir = $parent;
    foreach ($pathparts as $partno => $part) {
        //
        // Replace unwanted characters with underscore
        //
        $part = preg_replace("[^ 0-9a-zA-Z.]", "_", $part);
        if ($partno < $bottom_hierarchy) {
            
            $subdir .= DIRECTORY_SEPARATOR . $part;
            if (!file_exists($subdir)) {
                mkdir($subdir);
            }
        }
        else {
            //
            // If no grouping is specified (-g 0), we repeat the last element
            // of the URL to ensure the output is not written to a parent
            // folder with outputs from other resources sharing same parent.
            //
            $group = -1;
            if (isset($options["g"])) {
                $group = $options["g"];
            }
            else if (isset($options["group"])) {
                $group = $options["group"];
            }
            if (0 === intval($group)) {
                $subdir .= DIRECTORY_SEPARATOR . $part;
                if (!file_exists($subdir)) {
                    mkdir($subdir);
                }
            }
            
            //
            // Check for file extension
            //
            $ext = NULL;
            if (isset($options["e"])) {
                $ext = '.' . $options["e"];
            }
            else if (isset($options["group"])) {
                $ext = '.' . $options["ext"];
            }
            $local_path = $subdir . DIRECTORY_SEPARATOR . $part . $ext;
        }
    }
    goto END_PASS;
    
END_FAIL:

END_PASS:

    return $local_path;
}
