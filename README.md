# nodex

PHP CLI scripting utilities for accessing text w. cURL and manipulating w. DOM.
 
Copyright � 2019 Kuhrman Technology Solutions LLC
License GPLv3+: GNU GPL version 3

BACKGROUND

The original nodex was written as a batch job to extract content from web 
resources as HTML, transorm it and then insert it into new DOM documents.

The helper function proved to be useful on other batch jobs, so the original
code was modified to allow these scripts to use the helper functions and be 
loaded at runtime.

OVERVIEW

nodex is a DOM/XML manipulation utility written in PHP. The purpose of nodex is 
to separate batch job specific routines - such as data transformation, content 
creation and management, etc - from generic routines such as HTTP requests, 
opening and writing files, etc.

Some simple examples of how an administrator might use nodex:

* Extract a copy of all the native HTML within a given DOM element from every 
  document given by a list of URLs and embed it in a corresponding local HTML 
  document.

* Download all images for every document given by a list of URLs and copy them 
  to corresponding local folders

* Create a list of all the links in every document corresponding to a list of URLs

The examples above are only that and nodex is not limited to these. The specific 
rules and business process(es) for a batch job are provided by the administrator 
independent of the nodex core scripts. The administrator specifies which batch 
job(s) is(are) to be executed at runtime.

The nodex core is free software and licensed under GPL V3. Administrators are not 
required to share batch job scripts and rules in order to comply with the license 
but any changes to the core are governed by GPL.

SYNTAX

[user@server ~]$ php ./path/to/nodex.php [OPTIONS]

Run [user@server ~]$ php ./path/to/nodex.php -h (or --help) for basic help with
command line syntax and options. See https://bitbucket.org/kkuhrman/nodex/wiki/Home
for more detailed help.

DIRECTORY STRUCTURE

The default project tree structure is:

nodex -- in
         jobs
         log
         out
         rules
         src
         tmp

The 'nodex' in the tree above is the project root directory and the names under
this are the default sub-directories. The purpose of each sub-directory is 
given below.

in - This is the default directory nodex will search for any batch job input 
file(s).

jobs - This is the default directory nodex will search for batch job script 
file(s).

log - This is the default directory nodex will write any log files to.

out - This is the default directory nodex will write any batch job output to.

rules - This is the default directory nodex will search for rules file(s). 
Rules are explained in greater detail on the project Wiki. 
@see: https://bitbucket.org/kkuhrman/nodex/wiki/Rules

src - This directory contains the nodex core PHP source files, including nodex.php 

tmp - This is the directory nodex will read any temporary batch job output to.

OPTIONS

Options not requiring a value

    -h (--help) Print the nodex help message.

    -O (--overwrite) Forces overwrite of existing file(s) in event of name conflict.

    -v (--version) Print the current version number.

Options requiring a value

    -d (--directory) Specify parent of all input/output directories. Default is present working directory.

    -f (--file) Specify name of rules file to use. Default is empty.

    -i (--input) Specify a single, properly-formed URL as input. Default is empty.

    -j (--job) Specify file name of batch job script. Default is empty.

    -l (--log) Specify log event severity level. Default is LOG_ERROR.

    -o (--output) Specify file name of output. Default varies (see below).


Output files are written to the output directory. The default output directory
is ./nodex/out. Default behavior is to give output file(s) a file system 
friendly name derived from the input url(s), but this behavior can be changed 
in the rules file.


OBSOLETE OPTIONS

The following options were supported in early branches of nodex.php and must 
now be defined in a rules file (or now have an entirely different meaning as 
defined above).

Redefined:

-d	--dir	specify absolute or relative path of output directory

-f	--file	specify name of read-only input file after option

-l	--log	if passed will write log files to temporary dir

-r	--rules	(now option -f)

-t	--truncate	(now option -O)

-u	--url	(now option -i)

Moved to rules:

-e --extension  Specify output file extension. Default is no extension.

-g	--group	-g 0 will force nodex to place output(s) in dedicated folder(s)

USAGE

CLI script to be run from shell. e.g. [user@server ~]$ php ./path/to/nodex.php

Best practice is to separate core nodex project directories from those containing 
user batch job script, input, log, rules, output and temporary files by specifying  
a different  for the latter (see -d --directory option above). More granular 
control of directories can be specified in the rules file(s) (e.g. user wishes 
to use a directory structure other than  default nodex project tree).
